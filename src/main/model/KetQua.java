package main.model;

public class KetQua {
    String maSV;
    int diem;
    int lanThi;

    public KetQua(int diem, int lanThi, String maSV) {
        this.diem = diem;
        this.lanThi = lanThi;
        this.maSV = maSV;
    }

    public int getDiem() {
        return diem;
    }

    public void setDiem(int diem) {
        this.diem = diem;
    }

    public int getLanThi() {
        return lanThi;
    }

    public void setLanThi(int lanThi) {
        this.lanThi = lanThi;
    }
    
    public String getmaSV() {
        return maSV;
    }

    public void setmaSV(String maSV) {
        this.maSV = maSV;
    }
}
