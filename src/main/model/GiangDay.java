package main.model;

public class GiangDay {
    String maKhoa;
    String maCt;
    String maMon;
    int hocKy;
    int namHoc;

    public GiangDay(int hocKy, int namHoc, String maKhoa,String maCt,String maMon) {
        this.hocKy = hocKy;
        this.namHoc = namHoc;
        this.maKhoa = maKhoa;
        this.maCt = maCt;
        this.maMon = maMon;
    }

    public int getHocKy() {
        return hocKy;
    }

    public void setHocKy(int hocKy) {
        this.hocKy = hocKy;
    }

    public int getNamHoc() {
        return namHoc;
    }

    public void setNamHoc(int namHoc) {
        this.namHoc = namHoc;
    }
    
    public String getmaKhoa() {
        return maKhoa;
    }

    public void setmaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }
    
    public String getmaCt() {
        return maCt;
    }

    public void setmaCt(String maCt) {
        this.maCt = maCt;
    }
    
    public String getmaMon() {
        return maMon;
    }

    public void setmaMon(String maMon) {
        this.maMon = maMon;
    }
}
