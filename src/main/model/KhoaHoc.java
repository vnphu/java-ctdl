package main.model;

public class KhoaHoc {
    int maKhoaHoa;
    int namBD;
    int namKT;

    public KhoaHoc(int maKhoaHoa, int namBD, int namKT) {
        this.maKhoaHoa = maKhoaHoa;
        this.namBD = namBD;
        this.namKT = namKT;
    }

    public int getMaKhoaHoa() {
        return maKhoaHoa;
    }

    public void setMaKhoaHoa(int maKhoaHoa) {
        this.maKhoaHoa = maKhoaHoa;
    }

    public int getNamBD() {
        return namBD;
    }

    public void setNamBD(int namBD) {
        this.namBD = namBD;
    }

    public int getNamKT() {
        return namKT;
    }

    public void setNamKT(int namKT) {
        this.namKT = namKT;
    }
}
