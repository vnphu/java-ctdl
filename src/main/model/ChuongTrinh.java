package main.model;

public class ChuongTrinh {
    String maCTr;
    String tenCtr;

    public ChuongTrinh(String maCTr, String tenCtr) {
        this.maCTr = maCTr;
        this.tenCtr = tenCtr;
    }

    public String getMaCTr() {
        return maCTr;
    }

    public void setMaCTr(String maCTr) {
        this.maCTr = maCTr;
    }

    public String getTenCtr() {
        return tenCtr;
    }

    public void setTenCtr(String tenCtr) {
        this.tenCtr = tenCtr;
    }
}
