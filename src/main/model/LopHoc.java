package main.model;

public class LopHoc {
    String maLop;
    String maKhoa;
    String khoaHoc;
    String maCt;
    
    public LopHoc(String maLop, String maKhoa, String khoaHoc, String maCt) {
        this.maKhoa = maKhoa;
        this.khoaHoc = khoaHoc;
        this.maCt = maCt;
        this.maLop = maLop;
    }
    
    public String getmaLop() {
        return maLop;
    }

    public void setmaLop(String maLop) {
        this.maLop = maLop;
    }
    
    public String getmaKhoa() {
        return maKhoa;
    }

    public void setmaKhoa(String maKhoa) {
        this.maKhoa = maKhoa;
    }
    
    public String getkhoaHoc() {
        return khoaHoc;
    }

    public void setkhoaHoc(String khoaHoc) {
        this.khoaHoc = khoaHoc;
    }
    
    public String getmaCt() {
        return maCt;
    }

    public void setmaCt(String maCt) {
        this.maCt = maCt;
    }
}
